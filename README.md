# Sử dụng
```
npm install
npm run dev
```

# Link trang web đã deploy:
'https://recruiment-client.vercel.app/'

# Hướng Dẫn Sử Dụng Website Recruitment-Client

## Giới Thiệu
Chào mừng bạn đến với trang hướng dẫn sử dụng Website Recruitment-Client! Website này được thiết kế để giúp bạn tìm kiếm việc làm và quản lý quá trình ứng tuyển vào các công việc một cách dễ dàng. Dưới đây là một số hướng dẫn cơ bản về cách sử dụng trang web này.

## Đăng nhập và Đăng ký
- Trước tiên, bạn cần đăng nhập hoặc đăng ký tài khoản để truy cập vào các dịch vụ của trang web.
- Nếu bạn đã có tài khoản, hãy sử dụng thông tin đăng nhập của bạn để truy cập vào tài khoản cá nhân.
- Nếu bạn chưa có tài khoản, hãy chọn tùy chọn "Đăng ký" và điền đầy đủ thông tin cá nhân của bạn để tạo một tài khoản mới.

## Admin
### Tài khoản admin demo
```
admin: catphong2002@gmail.com
pass: 123123123
```

### Quản Lý Người Dùng
- Admin có quyền quản lý tất cả người dùng trên trang web.
- Bạn có thể thêm, chỉnh sửa hoặc xóa tài khoản của Recruiter và Interviewer.
- Admin cũng có thể thay đổi cài đặt chung của trang web.

### Quản Lý Công Việc
- Admin có quyền quản lý các công việc được đăng tải trên trang web.
- Bạn có thể thêm, chỉnh sửa hoặc xóa thông tin về các công việc và thể loại công việc.

## Recruiter
### Tài khoản recruiter demo
```
recruiter: lamldb0112@gmail.com
pass: 123123123
```
### Đăng Tin Tuyển Dụng
- Recruiter có quyền đăng tin tuyển dụng trên trang web.
- Bạn có thể tạo các công việc mới, thêm mô tả, yêu cầu và thông tin liên hệ.
- Ngoài ra, bạn có thể quản lý đơn ứng tuyển và tương tác với ứng viên.

## Interviewer
# Tài khoản interviewer demo
```
interview: interviewer@gmail.com
pass: 123123123
```
### Quản Lý Phỏng Vấn
- Interviewer có quyền quản lý lịch trình phỏng vấn của họ.
- Bạn có thể xem và quản lý danh sách ứng viên cần phỏng vấn, thời gian và địa điểm phỏng vấn.

## Candidate
### Tài khoản candidate demo
```
candidate: lethanhkhai12@gmail.com
pass: 123456789
```
### Tìm Việc Làm
- Sau khi đăng nhập, bạn có thể sử dụng tính năng tìm việc làm để tìm kiếm các công việc phù hợp với bạn.
- Bạn có thể sử dụng các bộ lọc như vị trí, ngành nghề, mức lương và thời gian làm việc để tùy chỉnh kết quả tìm kiếm của mình.
- Khi tìm thấy công việc phù hợp, bạn có thể nhấn vào công việc để xem chi tiết và nộp đơn ứng tuyển.

### Nộp Đơn Ứng Tuyển
- Khi bạn muốn ứng tuyển vào một công việc, hãy nhấn vào nút "Ứng tuyển" trên trang thông tin công việc.
- Bạn cần điền đầy đủ thông tin và tải lên hồ sơ của mình nếu yêu cầu.
- Sau khi nộp đơn ứng tuyển, bạn sẽ nhận được xác nhận và cập nhật về tình trạng ứng tuyển từ trang web.

### Quản Lý Tài Khoản
- Tại tài khoản cá nhân, bạn có thể cập nhật thông tin cá nhân, thay đổi mật khẩu và quản lý hồ sơ ứng viên của mình.
- Bạn cũng có thể theo dõi tình trạng các đơn ứng tuyển và thay đổi cài đặt thông báo.

## Liên Hệ Với Chúng Tôi
Nếu bạn gặp bất kỳ vấn đề hoặc cần sự hỗ trợ, xin vui lòng liên hệ với chúng tôi qua các thông tin liên hệ sau:

- Email: lethkhai@gmail.com
- Điện thoại: 0942594519

